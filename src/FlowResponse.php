<?php

namespace Spip\Bigup;

/**
 * Retours de la classe Flow
 * Indique le code de réponse http, et d’éventuelles données.
 */
class FlowResponse {
	public $code = 415;
	public $data = null;
	public function __construct($code, $data = null) {
		$this->code = $code;
		$this->data = $data;
	}
}
